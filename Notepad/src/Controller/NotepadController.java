package Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractButton;

import View.NotepadView;

public class NotepadController implements ActionListener{
	private NotepadView notepadView;
	
	public NotepadController(NotepadView notepadView) {
		this.notepadView = notepadView;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		String src = e.getActionCommand();
		if (src.equals("Exit")) {
			System.exit(0);
		}
		else if (src.equals("Zoom In")) {
			this.notepadView.zoomIn();
		}
		else if (src.equals("Zoom Out")) {
			this.notepadView.zoomOut();
		}
		else if (src.equals("Restore Default Zoom")) {
			this.notepadView.restoreDefaultZoom();
		}
		else if (src.equals("Times New Roman")) {
			this.notepadView.changeFontTimes();
		}
		else if (src.equals("Arival")) {
			this.notepadView.changeFontArival();
		}
		else if (src.equals("New")) {
			this.notepadView.repaintNew();
		}
		else if (src.equals("Word Wrap")) {
			AbstractButton checkBox = (AbstractButton) e.getSource();
			boolean check = checkBox.getModel().isSelected();
			if (check) {
				this.notepadView.enableWordWrap();
			}
			else {
				this.notepadView.disableWordWrap();
			}
		}
	}

}
